//
//  Topic+CoreDataProperties.swift
//  Pago
//
//  Created by Vlad Eduard Stern on 25/04/2018.
//  Copyright © 2018 Vlad Eduard Stern. All rights reserved.
//
//

import Foundation
import CoreData


extension Topic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Topic> {
        return NSFetchRequest<Topic>(entityName: "Topic")
    }

    @NSManaged public var messageCount: Int32
    @NSManaged public var followerCount: Int32
    @NSManaged public var titleText: String?
    @NSManaged public var lastPostDate: NSDate?
    @NSManaged public var userIcon: String?
    @NSManaged public var favorite: Bool
    @NSManaged public var following: Bool

}
