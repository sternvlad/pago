//
//  Item.swift
//  Pago
//
//  Created by Vlad Eduard Stern on 24/04/2018.
//  Copyright © 2018 Vlad Eduard Stern. All rights reserved.
//

import UIKit

class Item: NSObject {
    public var topic : Topic?
    public var forum : Forum?
    public var postDate : NSDate?
    
    public init (topic : Topic) {
        self.topic = topic
        if let lastPostDate = topic.lastPostDate {
            self.postDate = lastPostDate
        }
        self.forum = nil
    }
    
    public init (forum : Forum) {
        self.topic = nil
        self.forum = forum
        if let lastPostDate = forum.lastPostDate {
            self.postDate = lastPostDate
        }
    }
}
