//
//  Forum+CoreDataProperties.swift
//  Pago
//
//  Created by Vlad Eduard Stern on 25/04/2018.
//  Copyright © 2018 Vlad Eduard Stern. All rights reserved.
//
//

import Foundation
import CoreData


extension Forum {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Forum> {
        return NSFetchRequest<Forum>(entityName: "Forum")
    }

    @NSManaged public var messageCount: Int32
    @NSManaged public var topicCount: Int32
    @NSManaged public var titleText: String?
    @NSManaged public var lastPostDate: NSDate?
    

}
