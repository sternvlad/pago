//
//  Extentions.swift
//  Pago
//
//  Created by Vlad Eduard Stern on 25/04/2018.
//  Copyright © 2018 Vlad Eduard Stern. All rights reserved.
//

import Foundation

extension NSDate
{
    func toString() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let stringDate: String = dateFormatter.string(from: self as Date)
        return stringDate
    }
    
}
