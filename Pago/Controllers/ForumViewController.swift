//
//  ViewController.swift
//  Pago
//
//  Created by Vlad Eduard Stern on 24/04/2018.
//  Copyright © 2018 Vlad Eduard Stern. All rights reserved.
//

import UIKit

class ForumViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet internal var tableView : UITableView!;
    private var items : [Item] = [];
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private let contentGenerator = GenerateContent ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addRandomItem))
        if (items.count == 0){
            fillItems();
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items [indexPath.row];
        if (item.forum != nil) {
            let cell : ForumTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ForumTableViewCell") as! ForumTableViewCell
            if let forum = item.forum {
                if let lastPostDate = forum.lastPostDate {
                    cell.lblLastPostDate.text = "Last activity: \(String(describing: lastPostDate.toString()))"
                }
                cell.lblLittleText.text = forum.titleText
                cell.lblTopicCount.text = String (describing: forum.topicCount)
                cell.lblTopicCount.adjustsFontSizeToFitWidth = true
                cell.lblMessageCount.text = String (describing: forum.messageCount)
                cell.lblMessageCount.adjustsFontSizeToFitWidth = true
            }
            return cell
        }else {
            let cell : TopicTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TopicTableViewCell") as! TopicTableViewCell
            if let topic = item.topic {
                if let lastPostDate = topic.lastPostDate {
                    cell.lblLastPostDate.text = "Last activity: \(String(describing: lastPostDate.toString()))"
                }
                cell.lblLittleText.text = topic.titleText
                cell.followingStack.isHidden = (topic.following)
                cell.favoriteView.isHidden = (topic.favorite)
                cell.userImageView.image = UIImage (named: (topic.userIcon)!)
                cell.lblFollowersCount.text = String (describing: topic.followerCount)
                cell.lblFollowersCount.adjustsFontSizeToFitWidth = true
                cell.lblMessageCount.text = String (describing: topic.messageCount)
                cell.lblMessageCount.adjustsFontSizeToFitWidth = true
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let item = items [indexPath.row];
        if (item.topic != nil) {
            if let topic = item.topic {
                let removeFavoriteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: !topic.favorite ? "Remove favorite" : "Add to favorite", handler:{action, indexpath in
                    print("Remove favorite•ACTION")
                    item.topic?.favorite = !(topic.favorite)
                    self.reloadRowWithNewValues(indexPath: indexPath, item: item)
                });
                removeFavoriteAction.backgroundColor = UIColor(red: 0.949, green: 0.4588, blue: 0, alpha: 1);
                
                let unfollowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: !topic.following ? "Unfollow" : "Follow", handler:{action, indexpath in
                    print ("Unfollow•ACTION")
                    item.topic?.following = !(topic.following)
                    self.reloadRowWithNewValues(indexPath: indexPath, item: item)
                });
                unfollowAction.backgroundColor = UIColor(red: 0.4078, green: 0.5882, blue: 0.902, alpha: 1)
                return [unfollowAction, removeFavoriteAction]
            }
            return []
        }else {
            return []
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items [indexPath.row];
        if (item.topic == nil) {
            let newForumViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForumViewController") as! ForumViewController
            self.navigationController?.pushViewController(newForumViewController, animated: true);
        }
    }
    
    func reloadRowWithNewValues (indexPath : IndexPath, item : Item) {
        self.items.remove(at: indexPath.row)
        self.items.append(item)
        items.sort(by: { $0.postDate! as Date > $1.postDate! as Date })
        self.tableView.reloadData()
    }
    
    
    // filling items
    
    func fillItems () {
        var forums : [Forum] = [];
        var topics : [Topic] = []
        do {
            forums = try context.fetch(Forum.fetchRequest())
        }catch let error as NSError {
            print ("Could not fetch \(error), \(error.userInfo)")
        }
        do {
            topics = try context.fetch(Topic.fetchRequest())
        }catch let error as NSError {
            print ("Could not fetch \(error), \(error.userInfo)")
        }
        for forum in forums {
            items.append(Item(forum: forum))
        }
        for topic in topics {
            items.append(Item(topic: topic))
        }
        items.sort(by: { $1.postDate! as Date > $0.postDate! as Date })
        
    }
    
    // add elements to uitableview
    
    @objc func addRandomItem () {
        let type = Int(arc4random_uniform(2))
        let RANDOMDAYS = 1000;
        let RANDOMCOUNT = 99
        if (type == 1){
            let forum = contentGenerator.generateRandomForum(randomDays: RANDOMDAYS, randomCount: RANDOMCOUNT)
            items.append(Item (forum: forum))
        }else {
            let topic = contentGenerator.generateRandomTopic(randomDays: RANDOMDAYS, randomCount: RANDOMCOUNT)
            items.append(Item (topic: topic))
        }
        items.sort(by: { $1.postDate! as Date > $0.postDate! as Date })
        tableView.reloadData()
    }
    
    
}

