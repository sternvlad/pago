//
//  TopicTableViewCell.swift
//  Pago
//
//  Created by Vlad Eduard Stern on 24/04/2018.
//  Copyright © 2018 Vlad Eduard Stern. All rights reserved.
//

import UIKit

class TopicTableViewCell: UITableViewCell {

    @IBOutlet internal var userImageView : UIImageView!
    @IBOutlet internal var lblLittleText : UILabel!
    @IBOutlet internal var lblLastPostDate : UILabel!
    @IBOutlet internal var lblFollowersCount : UILabel!
    @IBOutlet internal var lblMessageCount : UILabel!
    @IBOutlet internal var followingStack : UIStackView!
    @IBOutlet internal var favoriteView : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
