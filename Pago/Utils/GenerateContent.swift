//
//  GenerateContent.swift
//  Pago
//
//  Created by Vlad Eduard Stern on 25/04/2018.
//  Copyright © 2018 Vlad Eduard Stern. All rights reserved.
//

import Foundation
import UIKit

public class GenerateContent {
    
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func generateRandomForum (randomDays : Int, randomCount : Int) -> Forum {
        let forum = Forum (entity: Forum.entity(), insertInto: context)
        forum.lastPostDate = generateRandomDate(daysBack: Int(arc4random_uniform(UInt32(randomDays))))
        forum.messageCount = Int32(Int(arc4random_uniform(UInt32(randomCount))))
        forum.titleText = generateRandomString(length: Int(arc4random_uniform(UInt32(randomCount))))
        forum.topicCount = Int32(Int(arc4random_uniform(UInt32(randomCount))))
        appDelegate.saveContext()
        return forum
    }
    
    func generateRandomTopic (randomDays : Int, randomCount : Int) -> Topic {
        let topic = Topic (entity: Topic.entity(), insertInto: context)
        topic.lastPostDate = generateRandomDate(daysBack: Int(arc4random_uniform(UInt32(randomDays))))
        topic.followerCount = Int32(Int(arc4random_uniform(UInt32(randomCount))))
        topic.messageCount = Int32(Int(arc4random_uniform(UInt32(randomCount))))
        topic.titleText = generateRandomString(length: Int(arc4random_uniform(UInt32(randomCount))))
        topic.favorite = arc4random_uniform(2) == 0
        topic.following = arc4random_uniform(2) == 0
        topic.userIcon = "profile.png"
        appDelegate.saveContext();
        return topic
    }
    
    func generateRandomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func generateRandomDate(daysBack: Int)-> NSDate?{
        let day = arc4random_uniform(UInt32(daysBack))+1
        let hour = arc4random_uniform(23)
        let minute = arc4random_uniform(59)
        
        let today = Date(timeIntervalSinceNow: 0)
        let gregorian  = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        var offsetComponents = DateComponents()
        offsetComponents.day = Int(day - 1)
        offsetComponents.hour = Int(hour)
        offsetComponents.minute = Int(minute)
        
        let randomDate = gregorian?.date(byAdding: offsetComponents, to: today, options: .init(rawValue: 0) )
        return randomDate! as NSDate
    }
}
